from bs4 import BeautifulSoup
import json
import urllib
import pickle

exercises = pickle.load( open( "save_excercises.p", "rb" ) )

data_ditct = {'type','main_muscle','equipment','mechanics', 'level', 'sport', 'force'}
allofit = []
start = 0






for stuff in exercises[123:]:
    start +=1
    print "\n\n\n"
    print start 
    print stuff[1] + "\n"
    i = {}

    try:
        r = urllib.urlopen(stuff[1]).read()
        soup = BeautifulSoup(r, "html.parser")
        infodict = {}


        ##Get details of excercise
        divTag = soup.find_all("div", {"id":"exerciseDetails"})
        for tag in divTag:
            tdTags = tag.find_all("span", {"class":"row"})
            
            for x in tdTags:
                y = x.get_text().replace("\n", "")
                name,data = y.split(':')
                infodict[name.strip()] = data.strip()
                
            
           


       ##Get main muscle used
        musclesworked = soup.find_all("div", {"class":"guideImage"})
        for m in musclesworked:
            muscle = m.a.get_text()
            muscle_image = m.img["src"]
          
            
            ##infodict['main_mucle'] = muscle
            infodict['mucle_heatmap'] = muscle_image

            
       ##Get Male howto images
        male_image_start = soup.find_all("div", {"id":"Male"})
        for mImage in male_image_start:

            Mend = mImage.find_all("div", {"class":"photoRight"})
            for mEi in Mend:
                 maleEnd = mEi.find("a", {"class":"thickbox"})["href"]
                 
            Mstart = mImage.find_all("div", {"class":"photoLeft"})
            for mSi in Mstart:
                 maleStart = mSi.find("a", {"class":"thickbox"})["href"]
                 
        infodict['male_image_start'] =  maleStart           
        infodict['male_image_end'] =  maleEnd
       
         


         ##Get Female howto images
        female_image_start = soup.find_all("div", {"id":"Female"})
        for fImage in female_image_start:
            
            Fend = fImage.find_all("div", {"class":"photoRight"})
            
            for fEi in Fend:
                 femaleEnd = fEi.find("a", {"class":"thickbox"})["href"]
                 
            Fstart = fImage.find_all("div", {"class":"photoLeft"})
            
            for fSi in Fstart:
                 femaleStart = fSi.find("a", {"class":"thickbox"})["href"]
                 
                 
            
        infodict['female_image_start'] =  femaleStart
        infodict['female_image_end'] =  femaleEnd


            

            
        ##Get guide to excercise
        guide  = soup.find_all("div", {"class":"guideContent"})
        for guidetag in guide:
            ollitag = guidetag.find_all("ol")
            for guidetag2 in ollitag:
                ollitag2 = guidetag2.find_all("li")
                
        infodict['guide'] = [x.text for x in ollitag2]


        ##Add link to end of excercise tree
        infodict['link'] = stuff[1]
        a = {stuff[0]:[infodict]}

        ##Add excersise to main tree
        allofit.append(a)

        print a
    except Exception as e:
        print e
        pass



with open('output_2_main.json', 'w') as outfile:
  json.dump(allofit, outfile)
